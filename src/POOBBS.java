import java.util.Scanner;

public class POOBBS{
	public static String name;
	public static void main(String[] args){
		System.out.println("Welcome to POOBBS, this is written by b00902098 Yu-Ting Kuo");
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Please enter your name: ");
		name = keyboard.next();
		System.out.println("Hello, " + name + ". Enjoy the world of POOBBS!");
		int choose;
		POODirectory root = new POODirectory("root", 0);//create root directory
		POOBBS bbs = new POOBBS();
		do{
			System.out.print("1.change the username  2.enter the root directory  0.terminate: ");
			choose = keyboard.nextInt();
			switch(choose){
				case 1:
					System.out.print("Please enter your name: ");
					name = keyboard.next();
					System.out.println("Hello, " + name + ". Enjoy the world of POOBBS!");
					break;
				case 2:
					bbs.inDirectory(root);
					break;
				case 0:
					System.out.println("Thank you, "+name+". Welcome to come again!");
					break;
				default:
					System.out.println("Please input 1, 2 or 0.");
			}
		}while(choose!=0);
	}
	public void inDirectory(POODirectory curDirectory){
		Scanner keyboard = new Scanner(System.in);
		System.out.print("1.show  2.enter a directory or board  3.add a new directory  4.add a new board  5.add a splitting line  6.delete  7.move  0.back: ");
		int choose = keyboard.nextInt();
		int position, dest, type;
		POOBoard item;
		String newName;
		while(choose!=0){
			switch(choose){
				case 1:
					if(curDirectory.length()==0) System.out.println("<empty>");
					else curDirectory.show();
					break;
				case 2:
					System.out.print("Please key in the position of the items you want to enter: ");
					position = keyboard.nextInt();
					item = curDirectory.getItem(position);
					if(item==null){
						System.out.println("The item at the position "+position+" doesn't exist.");
						break;
					}
					if(item.type==0){
						System.out.println("Enter the directory "+item.getName()+".");
						inDirectory((POODirectory)item);
					}
					else if(item.type==1){
						System.out.println("Enter the board "+item.getName()+".");
						inBoard(item);
					}
					else System.out.println("Splitting line can't be entered.");
					break;
				case 3:
					System.out.print("Add a new directory, please key in the name: ");
					newName = keyboard.next();
					curDirectory.add(new POODirectory(newName, 0));
					break;
				case 4:
					System.out.print("Add a new board, please key in the name: ");
					newName = keyboard.next();
					curDirectory.add(new POOBoard(newName, 1));
					break;
				case 5:
					System.out.println("Add a splitting line.");
					curDirectory.add_split();
					break;
				case 6:
					System.out.print("Delete the item, please enter the position of the item: ");
					position = keyboard.nextInt();
					curDirectory.del(position);
					break;
				case 7:
					System.out.print("Where is the item you want to move \"from\"? ");
					position = keyboard.nextInt();
					System.out.print("Where is the item you want to move \"to\"? ");
					dest = keyboard.nextInt();
					curDirectory.move(position, dest);
					break;
				default:
					System.out.println("Please input 1~6 or 0.");
			}
			System.out.print("1.show  2.enter a directory or board  3.add a new directory  4.add a new board  5.add a splitting line  6.delete  7.move  0.back: ");
			choose = keyboard.nextInt();
		}
	}
	public void inBoard(POOBoard curBoard){
		Scanner keyboard = new Scanner(System.in);
		System.out.print("1.show  2.read the article  3.add a new article  4.delete the article  5.move the article  0.back: ");
		int choose = keyboard.nextInt();
		int position,dest;
		String junk;
		POOArticle target;
		while(choose!=0){
			junk = keyboard.nextLine();
			switch(choose){
				case 1:
					if(curBoard.length()==0) System.out.println("No articles in the board "+curBoard.getName()+".");
					else curBoard.show();
					break;
				case 2:					
					System.out.print("Please enter the position of the article you want to read: ");
					position = keyboard.nextInt();
					target = curBoard.getArticle(position);
					/*try to judge whether the article exists or nnot*/
					if(target==null) System.out.println("The article at the position "+position+" doesn't exist.");
					else{
						System.out.println("You are reading the article \""+target.getTitle()+"\" now.");
						inArticle(target);
					}
					break;
				case 3:
					System.out.print("Post new article, please enter your title: ");
					String artTitle = keyboard.nextLine();
					System.out.print("Please enter the content of the article: ");
					String content = keyboard.nextLine();
					POOArticle article = new POOArticle(artTitle, name, content);
					curBoard.add(article);
					break;
				case 4:
					System.out.print("Delete the article, please enter the position of the article: ");
					position = keyboard.nextInt();
					curBoard.del(position);
					break;
				case 5:
					System.out.print("Where is the board you want to move \"from\"? ");
					position = keyboard.nextInt();
					System.out.print("Where is the item you want to move \"to\"? ");
					dest = keyboard.nextInt();
					curBoard.move(position,dest);
					break;
				default:
					System.out.println("Please input 1~4 or 0.");
			}
			System.out.print("1.show  2.read the article  3.add a new article  4.delete the article  5.move the article  0.back: ");
			choose = keyboard.nextInt();
		}
	}
	public void inArticle(POOArticle curArticle){
		Scanner keyboard = new Scanner(System.in);
		System.out.print("1.push  2.boo  3.arrow  4.show  5.list  0.back to board: ");
		int choose = keyboard.nextInt();
		String message,junk;
		while(choose!=0){
			junk = keyboard.nextLine();//discard the '\n'
			switch(choose){
				case 1:
					System.out.print("push "+name+": ");
					message = keyboard.nextLine();
					curArticle.push(message);
					break;
				case 2:
					System.out.print("boo  "+name+": ");
					message = keyboard.nextLine();
					curArticle.boo(message);
					break;
				case 3:
					System.out.print("-->  "+name+": ");
					message = keyboard.nextLine();
					curArticle.arrow(message);
					break;
				case 4:
					curArticle.show();
					break;
				case 5:
					curArticle.list();
					break;
				default:
					System.out.println("Please input 1~5 or 0");
			}
			System.out.print("1.push  2.boo  3.arrow  4.show  5.list  0.back to board: ");
			choose = keyboard.nextInt();
		}
	}
}
