public class POOBoard{
	public int count = 0;
	public int type;
	public String name;
	private POOArticle[] boardArticles;
	/**
	constructor with no arguments is for the splitting line
	*/
	public POOBoard(){
		type = 2;
		name = "split";
		boardArticles = null;
	}
	/**
	constructor to create a directory/board with the name and the type
	*/
	public POOBoard(String theName, int theType){
		name = theName;
		type = theType;
		if(type==1) boardArticles = new POOArticle[1024];//create a board
		else boardArticles = null;
	}
	public void add(POOArticle article){
	//append the article to the board
		if(count<1024)	boardArticles[count++] = article;
		else System.out.println("Sorry, the board is full.");
	}
	public void del(int pos){
	//delete the article at position pos
		if(boardArticles[pos]==null) System.out.println("The article at the position "+pos+" doesn't exist.");
		else{
			for(int i=pos;i<count-1;i++)//every article after it moves forward one position
				boardArticles[i] = boardArticles[i+1];
			boardArticles[--count] = null;
		}
	}
	public void move(int src, int dest){
	//move the article at position src to position dest
		if(boardArticles[src]==null) System.out.println("The article at the position "+src+" doesn't exist.");
		else if(dest>=count) System.out.println("Illegal destination at the position "+dest+".");
		else{
			POOArticle temp = boardArticles[src];
			int i;
			if(dest>src){//move forward one position
				for(i=src;i<dest;i++) boardArticles[i] = boardArticles[i+1];
			}
			else{//move backward one position
				for(i=src;i>dest;i--) boardArticles[i] = boardArticles[i-1];
			}
			boardArticles[dest] = temp;
		}
	}
	public int length(){
	//get the current number of articles in the board
		return count;
	}
	public void show(){
	//show the article titles of the board
		System.out.println("No. Author       title");
		for(int i=0;i<count;i++)
			System.out.printf("%3d %-12s %s\n",i,boardArticles[i].getAuthor(),boardArticles[i].getTitle());
	}
	public String getName(){
		return name;
	}
	/**
	get the article we want
	*/
	public POOArticle getArticle(int pos){
		return boardArticles[pos];
	}
}
