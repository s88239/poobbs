public class POOArticle{
	public static final int MAXEVAL = 80;
	private static int num = 0;
	private int id;
	private String title;
	private String author;
	private String content;
	private int evalCount;
	private String evalMessage;
	/**
	constructor for no arguments
	*/
	public POOArticle(){
		id = num++;
		title = "no title";
		author = "unknow";
		content = "empty";
		evalCount = 0;
		evalMessage = "-------------------------------------------------";
	}
	/**
	constructor the title, author, and content strings as arguments
	*/
	public POOArticle(String artTitle, String artAuthor, String artContent){
		id = num++;
		title = artTitle;
		author = artAuthor;
		content = artContent;
		evalCount = 0;
		evalMessage = "-------------------------------------------------";
	}
	/**
	push the article, and evaluation count add one
	*/
	public void push(String message){
		evalCount++;
		String pushLine = "push " + POOBBS.name + ": " + message;
		if(pushLine.length()>MAXEVAL) pushLine = pushLine.substring(0, MAXEVAL);
		evalMessage = evalMessage + "\n" + pushLine;
	}
	/**
	boo the article, and evaluation count substrac one
	*/
	public void boo(String message){
		evalCount--;
		String booLine = "boo  " + POOBBS.name + ": " + message;
		if(booLine.length()>MAXEVAL) booLine = booLine.substring(0, MAXEVAL);
		evalMessage = evalMessage + "\n" + booLine;
	}
	/**
	leave a message without changing evaluation count
	*/
	public void arrow(String message){
		String arrowLine = "-->  " + POOBBS.name + ": " + message;
		if(arrowLine.length()>MAXEVAL) arrowLine = arrowLine.substring(0, MAXEVAL);
		evalMessage = evalMessage + "\n" + arrowLine;
	}
	/**
	show the article
	*/
	public void show(){
		System.out.println("author: "+author);
		System.out.println("title: "+title);
		System.out.println("content: "+content);
		System.out.println(evalMessage);
	}
	/**
	list the information of the article
	*/
	public void list(){
		System.out.println("evaluation count: "+evalCount);
		System.out.println("ID: "+id);
		System.out.println("title: "+title);
		System.out.println("author: "+author);
	}
	public String getAuthor(){
		return author;
	}
	public String getTitle(){
		return title;
	}
}
