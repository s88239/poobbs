public class POODirectory extends POOBoard{
	private POOBoard[] items;
	/**
	constructor with no arguments is for splitting line
	*/
	public POODirectory(){
		super();
		items = null;
	}
	/**
	constructor with the name is to create a directory
	*/
	public POODirectory(String theName,int theType){
		super(theName,theType);
		items = new POOBoard[1024];
	}
	public void add(POOBoard board){
	//append the board to the directory
		if(count<1024) items[count++] = board;
		else System.out.println("Sorry, the directory is full.");
	}
	public void add(POODirectory dir){
	//append the directory to the directory
		if(count<1024) items[count++] = dir;
		else System.out.println("Sorry, the directory is full.");
	}
	public void add_split(){
	//append a splitting line to the directory
		if(count<1024) items[count++] = new POODirectory();//constructor with no arguments creates the object for splittimg line
		else System.out.println("Sorry, the directory is full.");
	}
	public void del(int pos){
	//delete the board/directory/splitting line at position pos
		if(items[pos]==null) System.out.println("The item at the position "+pos+" doesn't exist.");
		else{
			for(int i=pos;i<count-1;i++)//every article after it moves forward one position
				items[i] = items[i+1];
			items[--count] = null;
		}
	}
	public void move(int src, int dest){
	//move the board/directory/splitting line at position src to position dest
		if(items[src]==null) System.out.println("The item at the position "+src+" doesn't exist.");
		else if(dest>=count) System.out.println("Illegal destination at the position "+dest+".");
		else{
			POOBoard temp = items[src];
			int i;
			if(dest>src){//move forward one position
				for(i=src;i<dest;i++) items[i] = items[i+1];
			}
			else{//move backward one position
				for(i=src;i>dest;i--) items[i] = items[i-1];
			}
			items[dest] = temp;
		}
	}
	public int length(){
	//get the current number of items in the directory
		return count;
	}
	public POOBoard getItem(int pos){
		return items[pos];
	}
	public void show(){
	//show the board/directory titles and splitting lines of the directory
		System.out.println(" No. name         kind");
		for(int i=0;i<count;i++){
			if(items[i]==null) continue;
			else if(items[i].type==2) System.out.printf("%4d -----------------------\n",i);//splitting line
			else System.out.printf("%4d %-12s %-4s\n",i,items[i].name,items[i].type==0?"dir":"board");
		}
	}
}
